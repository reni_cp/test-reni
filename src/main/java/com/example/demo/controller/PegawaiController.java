package com.example.demo.controller;

import com.example.demo.model.Pegawai;
import com.example.demo.service.PegawaiService;
import com.example.demo.vo.PegawaiVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PegawaiController {
    @Autowired
    PegawaiService pegawaiService;


    @ApiOperation(value = "View a list of Pegawai", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/pegawai")
    public ResponseEntity<List<Pegawai>> getAllPegawai(){
        List<Pegawai> list = pegawaiService.getList();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "get of Pegawai", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved "),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/pegawai/{id}")
    public ResponseEntity<Pegawai> getPegawai(@PathVariable int id){
        Pegawai list = pegawaiService.getPegawaiById(id);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/pegawai")
    public ResponseEntity<String> addPegawai(@RequestBody PegawaiVO pegawaiVO){
        Boolean result = pegawaiService.addPegawai(pegawaiVO);
        if(result){
            return new ResponseEntity<>("Add Pegawai Berhasil", HttpStatus.OK);
        }
        return new ResponseEntity<>("Add Pegawai Gagal", HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "Update Pegawai by Id", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Pegawai"),
            @ApiResponse(code = 404, message = "Id Pegawai not found")
    }
    )
    @PutMapping({"/pegawai/{id}"})
    public ResponseEntity<String> updatePegawai(@RequestBody PegawaiVO pegawaiVO, @PathVariable int id) {
        Pegawai local = pegawaiService.getPegawaiById(id);
        if(local != null){
            Boolean result =pegawaiService.editPegawai(pegawaiVO, id);
            if(result){
                return new ResponseEntity<>("Edit Pegawai Berhasil", HttpStatus.OK);
            }
            return new ResponseEntity<>("Edit Pegawai Gagal", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>("Id tidak ditemukan", HttpStatus.NOT_FOUND);
        }


    }

    @ApiOperation(value = "Delete Pegawai by Id", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Pegawai"),
            @ApiResponse(code = 404, message = "Id Pegawai not found")
    }
    )
    @DeleteMapping({"pegawai/{id}"})
    public ResponseEntity<String> deletePegawaiByID(@PathVariable int id) {
        Pegawai local = pegawaiService.getPegawaiById(id);
        if(local != null) {
            Boolean result = pegawaiService.deletePegawai(id);
            if (result) {
                return new ResponseEntity<>("Delete Pegawai Berhasil", HttpStatus.OK);
            }
            return new ResponseEntity<>("Delete Pegawai Gagal", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>("Id tidak ditemukan", HttpStatus.NOT_FOUND);
        }
    }
}
