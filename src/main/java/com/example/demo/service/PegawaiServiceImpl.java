package com.example.demo.service;

import com.example.demo.model.Pegawai;
import com.example.demo.repo.PegawaiRepo;
import com.example.demo.vo.PegawaiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class PegawaiServiceImpl implements PegawaiService{

    @Autowired
    private PegawaiRepo pegawaiRepo;

    @Override
    public List<Pegawai> getList() {
        return pegawaiRepo.findAll();
    }

    @Override
    public Boolean addPegawai(PegawaiVO pegawaiVO) {
        Boolean result = false;
        Pegawai pegawai1= new Pegawai ();
        try {
            pegawai1.setNama(pegawaiVO.getNama());
            pegawai1.setJenisKelamin(pegawaiVO.getJenisKelamin());
            Pegawai peg = pegawaiRepo.save(pegawai1);
            result =true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public Boolean editPegawai(PegawaiVO pegawai, int id) {
        Boolean result = false;
        try {
            Pegawai pegLocal = new Pegawai(pegawai.getNama(), pegawai.getJenisKelamin());
            pegLocal.setId(id);
            pegawaiRepo.save(pegLocal);
            result = true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public Boolean deletePegawai(int id) {
        Boolean result= false;
        try {
            pegawaiRepo.deleteById(id);
            result =true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public Pegawai getById(int id) {
        Pegawai result = new Pegawai();
        try{
            result= pegawaiRepo.getOne(id);
            result.getNama();
        }catch (Exception e){
            System.out.println(e.getMessage());
            result = new Pegawai();
        }
        return result;
    }

    @Override
    public Pegawai getPegawaiById(int id) {
        return pegawaiRepo.getById(id);
    }

}
