package com.example.demo.service;

import com.example.demo.model.Pegawai;
import com.example.demo.vo.PegawaiVO;

import java.util.List;

public interface PegawaiService {
    List<Pegawai> getList();
    Boolean addPegawai(PegawaiVO pegawaiVO);
    Boolean editPegawai(PegawaiVO pegawai, int id);
    Boolean deletePegawai (int id);
    Pegawai getById(int id);
    Pegawai getPegawaiById(int id);
}
