package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name="tm_pegawai")
public class Pegawai {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", columnDefinition = "serial")
    private int id;

    @Column(name="nama")
    private String nama;

    @Column(name="jk")
    private String jenisKelamin;

    public Pegawai(){}
    public Pegawai(String nama, String jenisKelamin){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
}
