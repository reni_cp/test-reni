package com.example.demo.repo;

import com.example.demo.model.Pegawai;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PegawaiRepo  extends JpaRepository<Pegawai, Integer> {

    Pegawai getById(int id);
}
