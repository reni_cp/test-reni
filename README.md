# test-reni

test API:
http://localhost:8080/swagger-ui.html

CREATE TABLE public.tm_pegawai
(
    id integer NOT NULL DEFAULT nextval('tm_pegawai_id_seq'::regclass),
    nama character varying(100) COLLATE pg_catalog."default",
    jk character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT tm_pegawai_pkey PRIMARY KEY (id)
)